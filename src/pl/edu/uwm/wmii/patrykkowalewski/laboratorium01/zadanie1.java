package pl.edu.uwm.wmii.patrykkowalewski.laboratorium01;
import java.util.Scanner;
import java.lang.Math;


public class zadanie1 {


    public static void main(String[] args) {
        int n;
        double x[],y[],z[], a,b,c,d,e,f,g,h,j;
        Scanner in = new Scanner(System.in);


        System.out.println("Podaj liczbe elementow tablicy");
        n = in.nextInt();
        x = new double[n];
        y = new double[n];
        z = new double[n];
        for (int i=0; i<n; i++) {
            System.out.println("Podaj element a[" + (i+1) +"]");
            x[i] = in.nextDouble();
        }


        a = 0;
        for (int i=0; i<n; i++) {
            a += x[i];
        }

        b=1;
        for (int i=0; i<n; i++) {
            b = b*x[i];
        }

        c=0;
        for (int i=0; i<n; i++) {
            if(x[i]<0){
                x[i]=-1*x[i];
            }
            c = c+x[i];
        }

        d=0;
        for (int i=0; i<n; i++) {
            if(x[i]<0){
                x[i]=-1*x[i];
            }
            y[i]=x[i];
            y[i]=Math.sqrt(y[i]);
            d = d+y[i];
        }

        e=1;
        for (int i=0; i<n; i++) {
            if(x[i]<0){
                x[i]=-1*x[i];
            }
            e = e*x[i];
        }

        f=0;
        for (int i=0; i<n; i++) {
            z[i]=x[i];
            z[i]=z[i]*z[i];
            f = f+z[i];
        }

        g = 0;
        double g2=1;
        for (int i=0; i<n; i++) {
            g = g+x[i];
            g2 = g2*x[i];
        }

        h=0;
        for (int i=0; i<n; i++) {
            if(i%2==1) {
                h = h+x[i];}
            else {
                h = h-x[i];}

        }

        j=0;
        double silnia=1;
        for (int i=0; i<n; i++) {
            silnia = silnia * i;
            if (i % 2 == 0) {
                j = j + (x[i]/silnia);
            } else {
                j = j - (x[i]/silnia);
            }
        }


        System.out.println("(a) = " + a);
        System.out.println("(b) = " + b);
        System.out.println("(c) = " + c);
        System.out.println("(d) = " + d);
        System.out.println("(e) = " + e);
        System.out.println("(f) = " + f);
        System.out.println("(g) = " + g + " oraz " + g2);
        System.out.println("(h) = " + h);
        System.out.println("(i) = " + j);
    }

}