package pl.edu.uwm.wmii.patrykkowalewski.laboratorium01;

import java.util.Scanner;

public class zadanie2_2 {
    public static void main(String[]args)  {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj n:");
        int n = in.nextInt();
        double a;
        double wynik=0;
        for (int i = 1; i<=n; i++){
            System.out.println("podaj " + i + " liczbe:");
            a = in.nextDouble();
            if(a>0){
                wynik=wynik+2*a;
            }
        }
        System.out.println("wynik = " + wynik);
    }
}