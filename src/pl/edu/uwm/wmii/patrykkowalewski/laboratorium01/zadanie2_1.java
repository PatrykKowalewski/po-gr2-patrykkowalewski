package pl.edu.uwm.wmii.patrykkowalewski.laboratorium01;

import java.util.Scanner;
import java.lang.Math;

public class zadanie2_1 {
    public static void main(String[]args) {
        System.out.println("wybierz zadanie:");
        Scanner in = new Scanner(System.in);
        int choose = in.nextInt();
        System.out.println("podaj n:");
        int n = in.nextInt();
        int wynik;
        int a;
        switch (choose) {

            case 1:
                wynik = 0;
                for (int i = 1; i <= n; i++) {
                    System.out.println("podaj " + i + " liczbe:");
                    a = in.nextInt();
                    if (a % 2 == 1) {
                        wynik++;
                    }
                }
                System.out.println("wynik = " + wynik);
                break;

            case 2:
                wynik = 0;
                for (int i = 1; i <= n; i++) {
                    System.out.println("podaj " + i + " liczbe:");
                    a = in.nextInt();
                    if (a % 3 == 0 && a % 5 != 0) {
                        wynik++;
                    }
                }
                System.out.println("wynik = " + wynik);
                break;

            case 3:
                wynik = 0;
                double pier;
                for (int i = 1; i <= n; i++) {
                    System.out.println("podaj " + i + " liczbe:");
                    a = in.nextInt();
                    pier = Math.sqrt((double) a);
                    if (pier - (int) pier == 0 && (int) pier % 2 == 0) {
                        wynik++;
                    }
                }
                System.out.println("wynik = " + wynik);
                break;

            case 4:
                wynik = 0;
                int a1;
                int a2;
                System.out.println("podaj " + 1 + " liczbe:");
                a1 = in.nextInt();
                System.out.println("podaj " + 2 + " liczbe:");
                a = in.nextInt();
                for (int i = 3; i <= n; i++) {
                    a2 = a1;
                    a1 = a;
                    System.out.println("podaj " + i + " liczbe:");
                    a = in.nextInt();
                    if (a1 < ((a2 + a) / 2)) {
                        wynik++;
                    }
                }
                System.out.println("wynik = " + wynik);
                break;

            case 5:
                wynik = 0;
                int pot = 1;
                int silnia = 1;
                for (int i = 1; i <= n; i++) {
                    pot = pot * 2;
                    silnia = silnia * i;
                    System.out.println("podaj " + i + " liczbe:");
                    a = in.nextInt();
                    if (a > pot && a < silnia) {
                        wynik++;
                    }
                }
                System.out.println("wynik = " + wynik);
                break;

            case 6:
                wynik = 0;
                for (int i = 1; i <= n; i++) {
                    System.out.println("podaj " + i + " liczbe:");
                    a = in.nextInt();
                    if (i % 2 == 1 && a % 2 == 0) {
                        wynik++;
                    }
                }
                System.out.println("wynik = " + wynik);
                break;

            case 7:
                wynik = 0;
                for (int i = 1; i <= n; i++) {
                    System.out.println("podaj " + i + " liczbe:");
                    a = in.nextInt();
                    if (a % 2 == 1 && a >= 0) {
                        wynik++;
                    }
                }
                System.out.println("wynik = " + wynik);
                break;

            case 8:
                wynik = 0;
                for (int i = 1; i <= n; i++) {
                    System.out.println("podaj " + i + " liczbe:");
                    a = in.nextInt();
                    if (a < 0) {
                        a = -1 * a;
                    }
                    if (a < i * i) {
                        wynik++;
                    }
                }
                System.out.println("wynik = " + wynik);
                break;

            default:
                System.out.println("podano zly nr zadania");
        }
    }
}