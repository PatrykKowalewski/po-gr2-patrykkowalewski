package pl.edu.uwm.wmii.patrykkowalewski.laboratorium01;

import java.util.Scanner;

public class zadanie2_4 {
    public static void main(String[]args)  {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj n:");
        int n = in.nextInt();
        double a;
        double max;
        double min;
        System.out.println("podaj 1 liczbe:");
        a = in.nextDouble();
        min=a;
        max=a;
        for (int i = 2; i<=n; i++){
            System.out.println("podaj " + i + " liczbe:");
            a = in.nextDouble();
            if(a>max){
                max=a;
            }
            if(a<min){
                min=a;
            }
        }
        System.out.println("min = " + min);
        System.out.println("max = " + max);
    }
}
