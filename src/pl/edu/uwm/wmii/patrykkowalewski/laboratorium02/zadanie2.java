package pl.edu.uwm.wmii.patrykkowalewski.laboratorium02;
import java.util.Scanner;
import java.util.Random;


public class zadanie2 {


    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("podaj n z przedzialu od 1 do 100:");
        int n = in.nextInt();
        if (n < 1 || n > 100) {
            System.out.println("zly zakres");
            System.exit(0);
        }
        int[] a = new int[n];
        generuj(a);
        wypisz(a);
        ileNieparzystych(a, n);
        ileParzystych(a, n);
        ileDodatnich(a, n);
        ileUjemnych(a, n);
        ileZerowych(a, n);
        ileMaksymalnych(a, n);
        sumaDodatnich(a, n);
        sumaUjemnych(a, n);
        dlugoscMaksymalnegoCiaguDodatnich(a, n);
        signum(a, n);
        odwrocFragment(a,n);
    }

    public static void generuj(int[] tab) {
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(1999) - 999;
        }
    }

    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.print("");
    }

    public static int ileNieparzystych(int[] tab, int n) {
        int npar = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] % 2 != 0) {
                npar++;
            }
        }
        System.out.println("\nWartosci nieparzystych: " + npar);
        return npar;
    }

    public static int ileParzystych(int[] tab, int n) {
        int par = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] % 2 == 0) {
                par++;
            }
        }
        System.out.println("Wartosci parzystych: " + par);
        return par;
    }

    public static int ileDodatnich(int[] tab, int n) {
        int dod = 0;
        for (int i = 0; i < n; i++) {

            if (tab[i] > 0) {
                dod++;
            }

        }
        System.out.println("Wartosci dodatnich: " + dod);
        return dod;
    }

    public static int ileUjemnych(int[] tab, int n) {
        int uj = 0;
        for (int i = 0; i < n; i++) {

            if (tab[i] < 0) {
                uj++;
            }

        }
        System.out.println("Wartosci ujemnych: " + uj);
        return uj;
    }

    public static int ileZerowych(int[] tab, int n) {
        int zero = 0;
        for (int i = 0; i < n; i++) {

            if (tab[i] == 0) {
                zero++;
            }

        }
        System.out.println("Wartosci zerowych: " + zero);
        return zero;
    }

    public static int ileMaksymalnych(int[] tab, int n) {
        int maks = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] > maks) {
                maks = tab[i];
            }
        }
        System.out.println("Najwieksza wartosc tablicy: " + maks);
        return maks;
    }

    public static int sumaDodatnich(int[] tab, int n) {
        int dodatnie = 0;

        for (int i = 0; i < n; i++) {
            if (tab[i] >= 0) {
                dodatnie = dodatnie + tab[i];
            }
        }
        System.out.println("suma dodatnich: " + dodatnie);
        return dodatnie;
    }

    public static int sumaUjemnych(int[] tab, int n) {
        int ujemne = 0;

        for (int i = 0; i < n; i++) {
            if (tab[i] <= 0) {
                ujemne = ujemne + tab[i];
            }
        }
        System.out.println("suma ujemnych: " + ujemne);
        return ujemne;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab, int n) {
        int dlugosc = 0;
        int reset = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] >= 0) {
                dlugosc++;
            }
            if (dlugosc > reset) {
                reset = dlugosc;
            }
            if (tab[i] < 0) {
                dlugosc = 0;
            }
        }
        System.out.println("najdluzszy ciag liczb dodatnich: " + reset);
        return reset;
    }

    public static void signum(int[] tab, int n) {
        for (int i = 0; i < n; i++) {
            if (tab[i] >= 0) {
                tab[i] = 1;
            } else {
                tab[i] = -1;
            }
        }
        for (long el : tab) {
            System.out.print(el + " ");
        }
    }

    public static void odwrocFragment(int[] tab, int n) {
        int prawy = 0;
        int x;
        int y;
        int lewy = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("\nPodaj 'lewy' z zakresu od 0 do " + (n - 1) + ":");
        lewy = in.nextInt();
        System.out.println("Podaj 'prawy' z zakresu od 0 do " + (n - 1) + ":");
        prawy = in.nextInt();
        x = tab[lewy];
        y = tab[prawy];
        tab[prawy] = x;
        tab[lewy] = y;
        for (long el : tab) {
            System.out.print(el + " ");

        }

    }
}

