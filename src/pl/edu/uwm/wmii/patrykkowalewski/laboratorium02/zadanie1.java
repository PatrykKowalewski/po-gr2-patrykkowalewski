package pl.edu.uwm.wmii.patrykkowalewski.laboratorium02;
import java.util.Scanner;
import java.lang.Math;
import java.util.Random;


public class zadanie1 {
    public static void generuj(int[] tab, int max){
        Random r= new Random();
        for (int j = 0;j < tab.length; ++j){
            tab[j] = r.nextInt(max);
        }
    }
    public static void main(String[]args) {
        System.out.println("wybierz zadanie:");
        Scanner in = new Scanner(System.in);
        int nrzadania = in.nextInt();
        System.out.println("podaj n z przedzialu od 1 do 100:");
        int n = in.nextInt();
        if(n<1 || n>100)
        {
            System.out.println("Zly numer zadania,zamykam program");
            System.exit(0);
        }
        int wynik;
        int a;
        switch (nrzadania) {
            case 1:
                int par=0;
                int npar=0;
                int tab[]=new int[n];
                Random r= new Random();
                for (int j = 0;j < tab.length; ++j){
                    tab[j] = r.nextInt(1999)-999;
                }
                for (long el : tab){
                    System.out.print(el +" ");
                }

                for(int i=0;i<n;i++) {
                    if (tab[i]%2 == 0) {
                        par++;
                    }
                    else {
                        npar++;
                    }
                }
                System.out.println("Wartosci parzystych: " + par);
                System.out.println("Wartosci nieparzystych: " + npar);
                break;

            case 2:
                int nad=0;
                int pod=0;
                int zero=0;
                int tab1[]=new int[n];
                Random r1= new Random();
                for (int j = 0;j < tab1.length; ++j){
                    tab1[j] = r1.nextInt(1999)-999;
                }
                for (long el : tab1){
                    System.out.print(el +" ");
                }

                for(int i=0;i<n;i++) {
                    if (tab1[i]<0) {
                        pod++;
                    }
                    if (tab1[i]>0) {
                        nad++;
                    }
                    if(tab1[i]==0) {
                        zero++;
                    }
                }
                System.out.println("Wartosci wiekszych od 0: " + nad);
                System.out.println("Wartosci mniejszch od 0: " + pod);
                System.out.println("Wartosci rownych 0: " + zero);
                break;


            case 3:
                int maks=0;
                int razy=0;
                int tab2[]=new int[n];
                Random r2= new Random();
                for (int j = 0;j < tab2.length; ++j){
                    tab2[j] = r2.nextInt(1999)-999;
                }
                for (long el : tab2){
                    System.out.print(el +" ");
                }
                for(int i=0;i<n;i++){
                    if(tab2[i]>maks){
                        maks=tab2[i];
                    }
                }
                for(int i=0;i<n;i++){
                    if(tab2[i]==maks){
                        razy++;
                    }
                }
                System.out.println("Najwieksza wartosc tablicy: " + maks);
                System.out.println("Wystapila tyle razy: " + razy);
            case 4:
                int ujemne=0;
                int dodatnie=0;
                int tab3[]=new int[n];
                Random r3= new Random();
                for (int j = 0;j < tab3.length; ++j){
                    tab3[j] = r3.nextInt(1999)-999;
                }
                for (long el : tab3){
                    System.out.print(el +" ");
                }
                for(int i=0;i<n;i++){
                    if(tab3[i]<0){
                        ujemne=ujemne+tab3[i];
                    }
                    if(tab3[i]>=0){
                        dodatnie=dodatnie+tab3[i];
                    }
                }
                System.out.println("suma dodatnich: " + dodatnie);
                System.out.println("suma ujemnych: " + ujemne);
                break;

            case 5:
                int dlugosc=0;
                int reset=0;
                int tab4[]=new int[n];
                Random r4= new Random();
                for (int j = 0;j < tab4.length; ++j){
                    tab4[j] = r4.nextInt(1999)-999;
                }
                for (long el : tab4){
                    System.out.print(el +" ");
                }
                for(int i=0;i<n;i++){
                    if(tab4[i]>=0)
                    {
                        dlugosc++;
                    }
                    if(dlugosc>reset)
                    {
                        reset=dlugosc;
                    }
                    if(tab4[i]<0){
                        dlugosc=0;
                    }
                }
                System.out.println("najdluzszy ciag liczb dodatnich: " +reset);
            case 6:
                int tab5[]=new int[n];
                Random r5= new Random();
                for (int j = 0;j < tab5.length; ++j){
                    tab5[j] = r5.nextInt(1999)-999;
                }
                for (long el : tab5){
                    System.out.print(el +" ");
                }
                for(int i=0;i<n;i++){
                    if(tab5[i]>=0){
                        tab5[i]=1;
                    }
                    else{
                        tab5[i]=-1;
                    }

                }
                System.out.println("");
                for(long el : tab5) {
                    System.out.print(el + " ");
                }
                break;
            case 7:
                int prawy=0;
                int bank1;
                int bank2;
                int lewy=0;
                int tab6[]=new int[n];
                Random r6= new Random();
                for (int j = 0;j < tab6.length; ++j){
                    tab6[j] = r6.nextInt(1999)-999;
                }
                for (long el : tab6){
                    System.out.print(el +" ");
                }
                System.out.println("");
                System.out.println("Podaj 'lewy' z zakresu od 0 do "+(n-1)+":");
                lewy = in.nextInt();
                System.out.println("Podaj 'prawy' z zakresu od 0 do "+(n-1)+":");
                prawy = in.nextInt();
                bank1=tab6[lewy];
                bank2=tab6[prawy];
                tab6[prawy]=bank1;
                tab6[lewy]=bank2;
                for (long el : tab6){
                    System.out.print(el +" ");
                }

            default:
                System.out.println("zly nr zadania");
        }
    }
}
