package pl.edu.uwm.wmii.patrykkowalewski.laboratorium02;
import java.util.Random;
import java.util.Scanner;

public class zadanie3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n, k;
        System.out.println("Wszystkie podawane wartości mają być z przedziału od 1 do 10");
        System.out.println("Podaj m: ");
        m = in.nextInt();
        System.out.println("Podaj n: ");
        n = in.nextInt();
        System.out.println("Podaj k: ");
        k = in.nextInt();


        int[][] a = new int[m][n];
        System.out.println("Macierz a:");
        for (int[] row : a) {
            for (int column : row) {
                System.out.print(column + "    ");
            }
            System.out.println();
        }
        System.out.print("");
        int[][] b = new int[n][k];
        System.out.println("Macierz b:");
        for (int[] row : b) {
            for (int column : row) {
                System.out.print(column + "    ");
            }
            System.out.println();
        }
        System.out.println("Wynik mnozenia:");
        int[][] c = new int[m][k];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                for (int x = 0; x < n; x++) {
                    c[i][j] += a[i][x] * b[x][j];
                }
            }
        }
        for (int[] row : c) {
            for (int column : row) {
                System.out.print(column + "    ");
            }
            System.out.println();
        }
    }
}

