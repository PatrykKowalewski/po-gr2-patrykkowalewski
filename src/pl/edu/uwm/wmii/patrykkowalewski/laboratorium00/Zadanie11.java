package pl.edu.uwm.wmii.patrykkowalewski.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println("Spójrzcie, jaka wciąż sprawna,");
        System.out.println("jak dobrze się trzyma");
        System.out.println("w naszym stuleciu nienawiść");
        System.out.println("Jak lekko bierze wysokie przeszkody,");
        System.out.println("Jakie to łatwe dla niej - skoczyć, dopaść.");
    }
}