package pl.edu.uwm.wmii.patrykkowalewski.laboratorium03;
import java.util.Scanner;

public class zadanie1 {

    private static int countChar(String s, char c) {
        int wynik=0;

        for (int i=0; i<s.length();i++){
            if(s.charAt(i)==c)
                wynik++;
        }
        System.out.print("Litera '"+c+"' wystepuje: ");
        return wynik;

    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Podaj wyraz:");
        String s = in.next();
        System.out.println("Podaj litere do zliczenia:");
        char c = in.next().charAt(0);
        System.out.println(countChar(s,c));
    }
}