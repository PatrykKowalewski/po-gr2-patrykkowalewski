package pl.edu.uwm.wmii.patrykkowalewski.laboratorium03;
import java.util.Scanner;

public class zadanie1b {

    private static int countSubStr(String s, String SubStr) {
        int wynik=0, index=0;

        while ((index = s.indexOf(SubStr, index)) != -1 ){
            wynik++;
           index++;
        }
        System.out.print("wyraz '"+SubStr+"' pojawia sie w ilosci: ");
        return wynik;
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Podaj wyraz:");
        String s = in.next();
        System.out.println("Podaj 2 wyraz:");
        String SubStr = in.next();
        System.out.println(countSubStr(s,SubStr));
    }
}